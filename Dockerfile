ARG R_IMAGE=rocker/r-ver:latest
FROM rocker/r-ver:4.3.1
#ARG R_IMAGE
RUN echo "rocker/r-ver:4.3.1" > docker_image_version

## Build author
ARG AUTHOR="SK8 team"

## App name by default, the name is set at docker build dynamically using project name
ARG APP_NAME="matrixapp"
ENV APP_NAME matrixapp

## App version by default, the version is set at docker build dynamically using project commit version
ARG APP_VERSION="SK8"
ENV APP_VERSION ${APP_VERSION}


LABEL   org.opencontainers.image.authors="$AUTHOR" \
        org.opencontainers.image.licenses="GPL-2.0-or-later" \
      	org.opencontainers.image.source="https://forgemia.inra.fr/sk8/team/ressources/docker/dockerfile" \
      	org.opencontainers.image.vendor="SK8 Project" \
        org.opencontainers.image.description="A simple configuration to run a R-Shiny App" 


## Add here libraries dependencies
## there are more libraries that are needed here

RUN apt-get update && apt-get install -y --no-install-recommends \
    sudo \
    cmake \
    curl \
    default-jdk \
    emacs\
    g++ \
    gcc \
    gdebi-core \
    gfortran \
    git \
    libcairo2-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libclang-dev \
    libcurl4 \
    libcurl4-openssl-dev \
    libgit2-dev \
    libjpeg-dev \
    libnlopt-dev \
    libnode-dev \
    libpython2.7 \
    libssh2-1-dev \
    libssl-dev \
    libtiff5-dev \
    libv8-dev \
    libxml2 \
    libxml2-dev \
    libxt-dev \
    locales \
    make \
    openjdk-8-jdk \
    openssh-client \
    openssl \
    python3-pip \
    python3-virtualenv \
    r-base \
    r-cran-rjava \
    wget \
    zlib1g-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*


RUN R CMD javareconf


## Change timezone
ENV CONTAINER_TIMEZONE Europe/Paris
ENV TZ Europe/Paris

RUN sudo echo "Europe/Paris" > /etc/timezone
#RUN echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen \
#  && locale-gen fr_FR.UTF8 \
#  && /usr/sbin/update-locale LANG=fr_FR.UTF-8

#ENV LC_ALL fr_FR.UTF-8
#ENV LANG fr_FR.UTF-8

## Change shiny user rights
## USER shiny
RUN useradd -ms /bin/bash shiny
RUN passwd shiny -d

## R package dependencies
## Add here R packages dependencies
RUN Rscript -e "install.packages(c('shiny'), repos='https://cloud.r-project.org/', ask=FALSE, Ncpus=4)"

RUN Rscript -e "install.packages(c('renv','fs'), ask=FALSE, repos='https://cloud.r-project.org/')"
RUN  R -e "list.of.packages <- c('nloptr','shinythemes','shinyjs','ggplot2','shinyBS','plyr','shinyFiles','BH','data.table','DT','readr','colourpicker','shinydashboard','heatmaply','tools','devEMF','R.devices','FactoMineR','factoextra','gplots','V8','RColorBrewer','foreach','doParallel','gridExtra','plotly','dplyr','reticulate','Hmisc','devtools');new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,'Package'])];if(length(new.packages)) install.packages(new.packages, Ncpus=1);lapply(list.of.packages,function(x){suppressPackageStartupMessages(library(x,character.only=TRUE))})"
RUN  R -e 'install.packages("BiocManager"); BiocManager::install(c("AnnotationDbi","RDAVIDWebService"))'
RUN R -e 'install.packages(c("xlsx", "stringr"))'


## To get last version of packages
# comment it if you install specific R packages version
#RUN Rscript -e "update.packages(ask=FALSE)"


## Clean unnecessary libraries
RUN apt-get update && apt-get remove --purge -y \
    gdebi-core \
    libssl-dev \
    git \
    openssh-client \
    libssh2-1-dev \
    libgit2-dev \
#  && apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /home/shiny

## Copy the application into image

#COPY /work/ylippi/shinyapps/MA_Trix_AppMIA/ /root/MA_Trix_App

#COPY Rprofile.site /usr/lib/R/etc/

RUN mkdir -p /home/shiny/${APP_NAME} \
    && wget https://forgemia.inra.fr/ylippi/MA_Trix_App/-/archive/master/MA_Trix_App-master.tar.gz \
    && tar -xvf MA_Trix_App-master.tar.gz \
    && rm MA_Trix_App-master.tar.gz \
    && cp -r MA_Trix_App-master/* /home/shiny/${APP_NAME}
  
#ADD MA_Trix_App /home/shiny/${APP_NAME}
COPY myFooter2.html /home/shiny/${APP_NAME}

RUN chown -R shiny.shiny /home/shiny

USER shiny

EXPOSE 3838

ENV APP_NAME ${APP_NAME}

CMD ["sh", "-c", "Rscript -e \"shiny::runApp('/home/shiny/${APP_NAME}', port = 3838, host = '0.0.0.0' )\""]
#CMD ["R", "-e", "shiny::runApp('/root/MA_Trix_App')"]
